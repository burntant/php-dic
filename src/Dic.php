<?php

namespace Burntant\Dic;

/**
 * Basic IOC container
*/
class Dic
{
    protected $instances;

    protected $config; // Service, object creation defs

    protected $settings; // Constants

    public function __construct($config)
    {
        $this->config   = isset($config['services']) ? $config['services'] : null;
        $this->settings = isset($config['settings']) ? $config['settings'] : null;
    }

    public function getInstance($name)
    {
        if(! isset($this->instances[$name])) {
            $config = isset($this->config[$name]) ? $this->config[$name] : null;
            if($config === null) {
                throw new Exception('No config key given for ' . $name);
            }
            // If there is no class name, try the key name as a class.
            $class_name = isset($config['class']) ? trim($config['class'], '/') : $name;

            // Substitutor
            array_walk_recursive($config, array($this, 'substitute'));

            if(isset($config['args'])) {
                // Constructor injection
                $reflector = new \ReflectionClass($class_name);
                $instance = $reflector->newInstanceArgs($config['args']);
                unset($reflector);
            }
            else {
                $instance = new $class_name;
            }
            // Setter injection
            if(isset($config['setters'])) {
                foreach($config['setters'] as $key => $val) {
                    $setter_method = 'set' . $key;
                    call_user_func_array(array($instance, $setter_method), (array) $val);
                }
            }
            // Property injection
            if(isset($config['properties'])) {
                foreach($config['properties'] as $key => $val) {
                    $instance->$key = $val;
                }
            }
            $this->instances[$name] = $instance;
        }

        return $this->instances[$name];
    }

    public function getSetting($key)
    {
        return isset($this->settings[$key]) ? $this->settings[$key] : null;
    }

    public function substitute(&$val)
    {
        // Object dependency
        if(is_string($val) && strpos($val, '@') === 0) {
            $val = $this->getInstance(str_replace('@', '', $val)); // Object dependency
        }
        // Setting
        elseif(is_string($val) && strpos($val, '~') === 0) {
            $val =  $this->getSetting(str_replace('~', '', $val));
        }
    }

    public function setInstance($name, $instance)
    {
        if(isset($this->instances[$name])) {
            throw new Exception('Instance already registered under the key: ' . $name . ' ,(remove first).');
        }
        $this->instances[$name] = $instance;
    }

    public function removeInstance($name) {
        unset($this->instances[$name]);
    }
}