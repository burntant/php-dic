<?php
use Burntant\Dic\Dic;

$config = array(
    'settings' => array(
        'site.name'          => 'Foo Site',
        'site.tagline'       => '.',
        'site.description'   => 'Short paragraph of something.',
        'site.manager.name'  => 'Foo Bar',
        'site.manager.email' => 'foo.bar@example.com',
    ),
    'services' => array(
        'a' => array(
            'class' => '\Foo\Dba',
            'args' => array(
                array(
                    'host' => 'foo',
                    'username' => 'bar',
                    'password' => 'baz',
                    'dbname' => 'bat'
                ),
            )
        ),
        'b' => array(
            'class' => '\Bar\Mailer',
            'args' => array(
                array(
                    'hello world!'
                )
            ),
            'setters' => array(
                'Bat' => 'Boz!', // \Bar\Mailer::setBat('Boz!')
                'FromEmail' => '~site.manager.email' // This setting will be substituted
            )
        ),
        'c' => array(
            'class' => '\Baz\c',
            'args' => array(
                '@b', // Creation of c depends on a and b
                '@a'
            ),
            'properties' => array(
                'qux' => 'norf'
            )
        ),
    )
);

// Please configure autoloading for your classes.

// Create the container from a config
$dic = new Dic($config);

// Grab an object from the container
$c = $dic->getInstance('c');

// Replace object with another, you probably don't want to do this...
$d = new stdObject;
$dic->removeInstance('c');
$dic->setInstance('c', $d);

unset($dic);

// Example of overriding a config.
// We can do this before initiating the DIC.
$overrides = array(
    'services' => array(
        'b' => array(
            'setters' => array(
                'Bat' => 'Man!'
            )
        )
    )
);
$config = array_replace_recursive($config, $overrides);
$dic = new Dic($config);