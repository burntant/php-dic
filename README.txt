Name:
    Php Dic

Copyright:
    Grant Burton 2015.

Summary:
    Basic dependency injection container.

Why?:
    Authored when there weren't many of these to be found.

Features:
    Constructor, setter and property injection.
    Features may be added or removed at any time.

Caveats:
    There are probably far better projects out there.
    Don't expect support.

Docs:
    See example directory and the source.

Tests:
    Required.

Provided:
    As is.

License:
    Php Dic is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Php Dic is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Php Dic.  If not, see <http://www.gnu.org/licenses/>.